#ifndef __CLASSIFIER_H
#define __CLASSIFIER_H

#include <vector>
#include <queue>
#include "instance.h"
#include "profiler.h"

/*
 * A fast, online implementation of k-Nearest Neighbor that works if you can
 * produce an absolute distance for an instance (e.g. an evaluation function)
 */

struct Evaluation {
public:
    void *_data;
    double (*_score)(const Instance& instance, void *data);

    double score(const Instance& instance) const {
        return _score(instance, _data);
    }

    bool operator() (const Instance& i1, const Instance& i2) const {
        return _score(i1, _data) < _score(i2, _data);
    }
};

class Classifier {
private:
    Profiler *profiler;
    std::vector<Instance> instances;
    const Evaluation& evaluation;
    int k;
    int maxLabel;
    
    int findClosestIndex(const Instance& inst) const;
    std::vector<Instance *> findKNearest(const Instance& inst);

public:

//    Classifier(int k, Evaluation evaluation); 
    Classifier(Profiler *profiler, int k, const Evaluation& evaluation); 

    void addInstances(const std::vector<Instance>& instances);
    int classify(const Instance& instance);
	int pruneInstances(int thresh);
};
#endif //__CLASSIFIER_H
