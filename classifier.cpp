
#include <iostream>
#include <vector>
#include <map>
#include "math.h"
#include "classifier.h"
#include "instance.h"

using namespace std;

#undef PROFILER_START_FUN
#undef PROFILER_STOP_FUN
#undef PROFILER_START
#undef PROFILER_STOP

#define PROFILER_START_FUN(p)
#define PROFILER_STOP_FUN(p)
#define PROFILER_START(p,e)
#define PROFILER_STOP(p,e)
//binary search to find a match, or find the point where a match would be
// (where min crosses over max)
int Classifier::findClosestIndex(const Instance& inst) const {

    //first, find the closest
    PROFILER_START_FUN(profiler)
    int mid = -1;
    int min = 0;
    int max = instances.size() - 1;
    double instEval = evaluation.score(inst);
    while(max >= min) {
        mid = min + (max - min) / 2;
        //double dist = distance(inst, instances[mid]);
        double dist = instEval - evaluation.score(instances[mid]);
        if (dist == 0) {
            break;
        } else if (dist > 0) {
            min = mid + 1;
        } else {
            max = mid - 1;
        }
    }
    PROFILER_STOP_FUN(profiler);
    return mid;
}

std::vector<Instance *> Classifier::findKNearest(const Instance& inst) {
//    cout << "DONE: " << min << ", " << mid << ", " << max << ", " << instances.size() << endl;
    int mid = findClosestIndex(inst);
    PROFILER_START_FUN(profiler);
    std::vector<Instance *> kNearest;
    if (mid >= 0) {
        int size = instances.size();
        kNearest.reserve(this->k);
        double instEval = evaluation.score(inst);
        kNearest.push_back(&instances[mid]);
        for (int ii = mid - 1, jj = mid + 1;
             (ii >= 0 || jj < size) && kNearest.size() < k;
             ) {
            if (ii < 0 && jj < size) {
                kNearest.push_back(&instances[jj]);
                jj++;
            } else if (ii >= 0 && jj >= size) {
                kNearest.push_back(&instances[ii]);
                ii--;
            } else {
    //            cout << ii << "," << jj << "," << size << endl;
                double iiEval   = evaluation.score(instances[ii]);
                double jjEval   = evaluation.score(instances[jj]);
                
                double distii = fabs(instEval - iiEval);
                double distjj = fabs(instEval - jjEval);
                if (distii < distjj) {
                    kNearest.push_back(&instances[ii]);
                    ii--;
                } else {
                    kNearest.push_back(&instances[jj]);
                    jj++;
                }
            }
        }
    }
#if 0
    const Instance **kNearest = new const Instance*[k];
    int inx = 0;
    if (mid >= 0) {
        kNearest[inx++] = &instances[mid];
        int size = instances.size();
        for (int ii = mid - 1, jj = mid + 1; ii >= 0 && jj < size && inx < k; ) {
            double distii = 0; //fabs(distance2(inst, instances[ii], maxA, maxB));
            double distjj = 0; //fabs(distance2(inst, instances[jj], maxA, maxB));
            if (distii < distjj) {
                kNearest[inx++] = &instances[ii];
                ii--;
            } else {
                kNearest[inx++] = &instances[jj];
                jj++;
            }
        }
    }
//    vec.assign(kNearest, kNearest + k);
#endif
    PROFILER_STOP_FUN(profiler);
    return kNearest;
}


Classifier::Classifier(Profiler *profiler, int k, const Evaluation& evaluation) : profiler(profiler), k(k), evaluation(evaluation) {
    
}

void Classifier::addInstances(const std::vector<Instance>& instances) {
    this->instances.insert(this->instances.end(), instances.begin(), instances.end());
    std::sort(this->instances.begin(), this->instances.end(), evaluation);
    this->maxLabel = -1;
    for (std::vector<Instance>::const_iterator it = this->instances.begin();
         it != this->instances.end();
         it++) {
        if (it->getLabel() > maxLabel) {
            maxLabel = it->getLabel();
        }
    }
#ifdef DEBUG
    cout << this->instances.size() << " instances" << endl;
#endif
}


int Classifier::classify(const Instance& instance) {
    std::vector<Instance *> kNearest = findKNearest(instance);
   PROFILER_START(profiler, mostFrequentLabel);
//    std::map<int, int> histogram;
    int label = -1;
    int maxCount = 0;
    int size = kNearest.size();
    int *histogram = new int[this->maxLabel + 1];
    memset(histogram, 0, (this->maxLabel + 1) * sizeof(int));
    for (int ii = 0; ii < size; ii++) {
        Instance *inst = kNearest[ii];
        int count = ++histogram[inst->getLabel()];
		inst->markUsed();
        if (count > maxCount) {
            label = inst->getLabel();
            maxCount = count;
        }
    }
    delete [] histogram;
    PROFILER_STOP(profiler, mostFrequentLabel);
    return label;
}


int Classifier::pruneInstances(int thresh) {
	std::vector<Instance> newVec;
	for (std::vector<Instance>::iterator it = this->instances.begin();
         it != this->instances.end();
		 it++) {
		 if (it->getUses() >= thresh) {
			 newVec.push_back(*it);
		 }
		 it->clearUses();
	}
	this->instances = newVec;
	return newVec.size();
}
